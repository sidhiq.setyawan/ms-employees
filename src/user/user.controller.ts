import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import userService from './user.service';
import {
  UserListResponseValidator,
  createUserRequestValidator,
  UserResponseValidator
} from './user.validator';
import { IUserRequest } from './user.interface';

const getUser: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/users',
  options: {
    description: 'Get the list of users',
    notes: 'Get the list of users',
    tags: ['api', 'users'],
    response: {
      schema: UserListResponseValidator
    },
    handler: () => {
      return userService.getUsers();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Users retrieved'
          }
        }
      }
    }
  }
};
const createUser: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/users',
  options: {
    description: 'Create new user',
    notes: 'All information must valid',
    validate: {
      payload: createUserRequestValidator
    },
    response: {
      schema: UserResponseValidator
    },
    tags: ['api', 'users'],
    handler: async (
      hapiRequest: IUserRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newUser = await userService.createUser(hapiRequest.payload);
      return hapiResponse.response(newUser).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'User created.'
          }
        }
      }
    }
  }
};
const deleteUser: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/users',
  options: {
    description: 'Delete users',
    notes: 'Delete users',
    tags: ['api', 'users'],
    response: {
      schema: UserListResponseValidator
    },
    handler: async (
      hapiRequest: IUserRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const deleteUser = await userService.deleteUser(hapiRequest.payload);
      return hapiResponse.response(deleteUser).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Users retrieved'
          }
        }
      }
    }
  }
};
const updateUser: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/users',
  options: {
    description: 'Get the list of users',
    notes: 'Get the list of users',
    tags: ['api', 'users'],
    response: {
      schema: UserListResponseValidator
    },
    handler: async (
      hapiRequest: IUserRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newUser = await userService.updateUser(hapiRequest.payload);
      return hapiResponse.response(newUser).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Users retrieved'
          }
        }
      }
    }
  }
};

const userController: hapi.ServerRoute[] = [
  getUser,
  createUser,
  deleteUser,
  updateUser
];
export default userController;
