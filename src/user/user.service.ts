import { ERROR_CODE } from '../common/errors';

import userRepository from './user.repository';
import { IUser } from './user.interface';
import { AppError } from '../errors/AppError';

const getUsers = () => {
  return userRepository.get();
};

const createUser = async (user: IUser) => {
  const existingUser = await userRepository.getFirstByName(user.name);
  if (existingUser) {
    throw new AppError(ERROR_CODE.USER_NAME_EXISTED);
  }
  return userRepository.create(user);
};

const deleteUser = async (user: IUser) => {
  return userRepository.del(user);
};

const updateUser = async (user: IUser) => {
  const existingUser = await userRepository.getFirstByName(user.name);
  if (existingUser) {
    throw new AppError(ERROR_CODE.USER_NAME_EXISTED);
  }
  return userRepository.update(user);
};

const userService = {
  getUsers,
  createUser,
  deleteUser,
  updateUser
};
export default userService;
